/**
 * @function _reRenderSubComponent
 *
 * It is recursive function helper
 * Search for the sub compoennt recursively
 *
 * @param {*} element : html element
 * @param {*} pathsToSubCompoent: array
 */
const _reRenderSubComponent = (element, pathsToSubCompoent) => {
  if (pathsToSubCompoent.length === 0) {
    return element;
  }

  const current = pathsToSubCompoent.shift();

  const childElement =
    element && element.shadowRoot && element.shadowRoot.querySelector(current);

  if (childElement === null) {
    return `${current} not found`;
  }

  return _reRenderSubComponent(childElement, pathsToSubCompoent);
};

/**
 * @EXPORT
 * @function reRenderSubComponent
 *
 * 1/ recursively finding the subcompoent
 *
 * 2a/ if NOT found, console.error
 * 2b/ if FOUND, re-render the subcomponent
 *
 * @param {*} element: html element
 * @param {*} pathToSubCompoent : string
 */
export const reRenderSubComponent = (element, pathToSubCompoent) => {
  const targetElement = _reRenderSubComponent(
    element,
    pathToSubCompoent.split(" ")
  );

  if (typeof targetElement === "string") {
    console.error(targetElement);
  } else {
    targetElement.requestUpdate();
  }
};
