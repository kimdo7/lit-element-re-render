![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)
![KimDo7](https://img.shields.io/badge/KimDo7-red.svg)

### What is this repository for?

- Quick summary:
  Lit Element has a lazy rendering to improve performance; however, the developer's skill requires a deep understanding of memory allocation. With this extremely lightweight library, it will re-render the only request nested subcomponent. The main benefit of this library, it only re-render the component needed without re-render the whole application.

- Version `1.0.0`

### Installation

`npm i lit-element-re-render --save`

Then

```js
import { reRenderSubComponent } from "lit-element-re-render";
```

### Sample 1

```html
<!-- HTML -->
<grand-parent>
  <parent>
    <child></child>
  </parent>
</grand-parent>
```

```js
// LitELement from grand-parent level
// this is current grand-parent 
reRenderSubComponent(this, "parent child");
```

### Sample 2

```html
<!-- HTML -->
<grand-parent>
  <parent id='parent-id'>
    <child></child>
  </parent>
</grand-parent>
```

```js
// LitELement from grand-parent level
// Note: it uses recursive querySelector

reRenderSubComponent(this, "#parent-id child");
```

### Sample 3

```html
<!-- HTML -->
<grand-parent>
  <parent class='parent-class'>
    <child></child>
  </parent>
</grand-parent>
```

```js
// LitELement from grand-parent level
// Note: it uses recursive querySelector
// It may works if it is a unique html class or it is the first one
reRenderSubComponent(this, ".parent-class child");
```

### Who do I talk to?

- Repo owner or admin: `Kim Do`
- Linkedin: [@kimdo7](https://www.linkedin.com/in/kimdo7/)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)
